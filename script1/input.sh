#!/bin/bash
echo "Please type 'quit' to exit."
# Prompt the user with "Enter a single word> " and
# read the value into variable "x"
touch input.txt
while read -p "Enter a single word> " x
do
  if [ "${x}" == "quit" ]; then
    cat input.txt
    rm -rf input.txt
    break
  fi
  if [ "${x}" == "abort" ]; then
    exit
  fi
  echo $x>>/home/sachin/deepali/input.txt #make sure you change this path to your input.txt file

done
