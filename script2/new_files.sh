#!/bin/bash
echo "Please type 'quit' or 'abort' to exit."
# Prompt the user with "Enter the File Name> " and
# read the value into variable "x"

while read -p "Enter the file name> " x
do
  
  if [ "${x}" == "quit" ]; then
    break
  fi
  if [ "${x}" == "abort" ]; then
    exit
  fi
  touch $x
  echo "File Created"

done
